package club.web51.classroom_tool.DO.collDO;

import club.web51.classroom_tool.DO.baseDO.baseObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

/**
 * @Classname Academy
 * @Description TODO
 * @Date 2020/10/27 22:21
 * @Created by HanYuHao
 */
@Entity
public class Academy extends baseObject {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    @NotEmpty(message = "学院名称不能为空")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package club.web51.classroom_tool.DO.collDO;

import club.web51.classroom_tool.DO.baseDO.baseObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @Classname Major
 * @Description TODO
 * @Date 2020/10/27 22:22
 * @Created by HanYuHao
 */
@Entity
public class Major extends baseObject {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    @NotNull(message = "名字不能为空")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package club.web51.classroom_tool.DO.dataDO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Group_Student
 * @Description TODO
 * @Date 2020/11/10 13:52
 * @Created by HanYuHao
 */
@Entity
public class Group_Student {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 学生ID
     */
    @Column
    private Long sid;
    /**
     * 组别ID
     */
    @Column
    private Long gid;
}

package club.web51.classroom_tool.DO.dataDO;

import club.web51.classroom_tool.DO.baseDO.baseObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname SignRecord_Teacher
 * @Description TODO
 * @Date 2020/10/29 14:42
 * @Created by HanYuHao
 */
@Entity
public class SignRecord_Data extends baseObject {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 学生ID
     */
    @Column
    private Long sid;
    /**
     * 记录ID
     */
    @Column
    private String bid;
    /**
     * 状态
     */
    @Column
    private Integer status;
    /**
     * 位置信息
     */
    @Column
    private String location;
    /**
     * 图片信息
     */
    @Column
    private String photo_url;
    /**
     * 手势信息
     */
    @Column
    private String handinfo;

    public SignRecord_Data() {
    }

    public SignRecord_Data(Long id, Long sid, String bid, Integer status, String location, String photo_url, String handinfo) {
        this.id = id;
        this.sid = sid;
        this.bid = bid;
        this.status = status;
        this.location = location;
        this.photo_url = photo_url;
        this.handinfo = handinfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getHandinfo() {
        return handinfo;
    }

    public void setHandinfo(String handinfo) {
        this.handinfo = handinfo;
    }
}

package club.web51.classroom_tool.DO.dataDO;

import club.web51.classroom_tool.DO.baseDO.baseObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Classname Group_Clazz
 * @Description TODO 组别/课程 多对多记录类
 * @Date 2020/11/10 13:24
 * @Created by HanYuHao
 */
@Entity
public class Group_Clazz extends baseObject {
    @Id
    @GeneratedValue
    private Long id;
    /**
     * 组别ID
     */
    @Column
    private Long gid;
    /**
     * 课程ID
     */
    @Column
    private Long cid;
}

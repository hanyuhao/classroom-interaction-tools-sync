package club.web51.classroom_tool.DO.sysDO;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Classname Setting
 * @Description 系统/全局设置
 * @Date 2020/11/2 14:53
 * @Created by HanYuHao
 */
@Entity
@Table(name = "SYSTEM_SETTING")
public class Setting implements Serializable {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /**
     * 项目名
     */
    @Column(unique = true)
    private String name;
    /**
     * 项目值
     */
    @Column
    private String value;
    /**
     * 查询权限
     */
    @Column
    private String root;
    /**
     * 操作权限
     */
    @Column
    private String admin_root;
    @CreationTimestamp
    @Column(updatable = false)
    private Date createTime;
    @UpdateTimestamp
    private Date updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getAdmin_root() {
        return admin_root;
    }

    public void setAdmin_root(String admin_root) {
        this.admin_root = admin_root;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

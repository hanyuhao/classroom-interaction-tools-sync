package club.web51.classroom_tool.DO.baseDO;

import club.web51.classroom_tool.Enums.StatusEnum;
import org.springframework.data.annotation.Persistent;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Objects;

/**
 * @Classname baseObject
 * @Description TODO
 * @Date 2020/11/10 13:43
 * @Created by HanYuHao
 */
@MappedSuperclass
public class baseObject {
    /**
     * 枚举类可定义的状态
     */
    private Integer status;
    /**
     * 为无状态定义为正常
     */
    @PrePersist
    private void setDefaultStatus(){
        if(Objects.isNull(status)){
            status = StatusEnum.OK.getStatus();
        }
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

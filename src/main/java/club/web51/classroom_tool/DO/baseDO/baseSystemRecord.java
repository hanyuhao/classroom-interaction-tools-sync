package club.web51.classroom_tool.DO.baseDO;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Classname baseSystemRecords
 * @Description TODO
 * @Date 2020/10/27 20:18
 * @Created by HanYuHao
 */
@MappedSuperclass
public class baseSystemRecord {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String bid;
    //记录类型ID
    @Column
    @NotNull(message = "类型不能为空")
    private Long typeId;
    //记录显示名
    @Column

    private String name;
    //注意事项
    @Column
    private String note;
    /**
     * 状态
     */
    @Column
    private Integer status;
    /**
     * 创建时间
     */
    @CreationTimestamp
    @Column(updatable = false)
    private Date CreateTime;
    /**
     * 修改时间
     */
    @UpdateTimestamp
    private Date UpdateTime;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date createTime) {
        CreateTime = createTime;
    }

    public Date getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(Date updateTime) {
        UpdateTime = updateTime;
    }
}

package club.web51.classroom_tool.DO.baseDO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Random;

/**
 * @Classname basePeople
 * @Description TODO
 * @Date 2020/10/27 20:16
 * @Created by HanYuHao
 */
@MappedSuperclass
public class basePeople extends baseObject{
    @Id
    @GeneratedValue
    private Long id;
    @NotEmpty(message = "姓名不能为空")
    @Column
    private String name;
    @NotNull(message = "手机号必须填写")
    @Column(length = 11)
    private Long telephone;
    @Column
    @Pattern(regexp = "[男女]",message = "性别只能是 男 或 女 ！")
    private String sex;
    @Column(unique = true)
    @NotEmpty(message = "用户名不能为空")
    private String username;
    @JsonIgnore
    @Column
    @Size(max = 16,min = 8,message = "密码长度在8-16位之间！")
    private String password;
    @Column
    private String location;
    @JsonIgnore
    @Column
    private String salt;
    /**
     * 为Salt字段预生成一个随机的salt
     */
    @PrePersist
    public void preSetSalt(){
        if(getSalt()!=null){
            return;
        }
        /**
         * 指定随机字段长度
         */
        int length = 6;
        String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<length;i++){
            int number=random.nextInt(62);
            sb.append(str.charAt(number));
        }
        setSalt(sb.toString());
    }

    public Long getTelephone() {
        return telephone;
    }

    public void setTelephone(Long telephone) {
        this.telephone = telephone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }


    @Override
    public String toString() {
        return "basePeople{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", telephone=" + telephone +
                ", sex='" + sex + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", location='" + location + '\'' +
                ", salt='" + salt + '\'' +
                '}';
    }
}


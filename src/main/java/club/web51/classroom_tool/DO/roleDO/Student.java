package club.web51.classroom_tool.DO.roleDO;

import club.web51.classroom_tool.DO.baseDO.basePeople;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Classname Student
 * @Description TODO
 * @Date 2020/10/27 20:31
 * @Created by HanYuHao
 */
@Entity
public class Student extends basePeople implements Serializable {
    @Column
    private Long sid;
    @Column
    private Long academy_id;
    @Column
    private Long major_id;



    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getAcademy_id() {
        return academy_id;
    }

    public void setAcademy_id(Long academy_id) {
        this.academy_id = academy_id;
    }

    public Long getMajor_id() {
        return major_id;
    }

    public void setMajor_id(Long major_id) {
        this.major_id = major_id;
    }

    @Override
    public String toString() {

        return "Student{" +
                super.toString()+
                "sid=" + sid +
                ", academy_id=" + academy_id +
                ", major_id=" + major_id +
                '}';
    }
}

package club.web51.classroom_tool.DO.roleDO;

import club.web51.classroom_tool.DO.baseDO.basePeople;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @Classname Admin
 * @Description TODO
 * @Date 2020/11/1 17:29
 * @Created by HanYuHao
 */
@Entity
public class Admin extends basePeople implements Serializable {
    @UpdateTimestamp
    private Date last_mod_time;

    public Date getLast_mod_time() {
        return last_mod_time;
    }

    public void setLast_mod_time(Date last_mod_time) {
        this.last_mod_time = last_mod_time;
    }
}

package club.web51.classroom_tool.DO.roleDO;

import club.web51.classroom_tool.DO.baseDO.basePeople;
import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;

/**
 * @Classname Teacher
 * @Description TODO
 * @Date 2020/10/27 20:32
 * @Created by HanYuHao
 */
@Entity
public class Teacher extends basePeople implements Serializable {
    @Column
    @GeneratedValue
    private Long tid;
    @Column
    private Long major_id;
    @Column
    private Long academy_id;

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getMajor_id() {
        return major_id;
    }

    public void setMajor_id(Long major_id) {
        this.major_id = major_id;
    }

    public Long getAcademy_id() {
        return academy_id;
    }

    public void setAcademy_id(Long academy_id) {
        this.academy_id = academy_id;
    }
}

package club.web51.classroom_tool.Controllers.RecordController;

import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.SignRecordRepository;
import club.web51.classroom_tool.Utils.MsgUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;
import club.web51.classroom_tool.DO.dataDO.SignRecord;
import club.web51.classroom_tool.Services.SignRecordService;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Api(value = "签到数据模块",tags={"签到数据操作接口"})
@RestController
@CrossOrigin
@RequestMapping("/signRecord")

public class SignRecordController {

    final
    SignRecordService signRecordService;
    @Resource
    SignRecordRepository signRecordRepository;

    public SignRecordController(SignRecordService signRecordService) {
        this.signRecordService = signRecordService;
    }

    /**
     * 根据SignRecord的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param signRecord 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     @ApiOperation("动态查询，可输入对应对象的一个或多个属性")
     ResponseEntity<Object> search(SignRecord signRecord, Pageable pageable) {
             return signRecordService.search(signRecord, pageable);
     }
     @PostMapping
     @ApiOperation("创建一个签到")
//     @RequiresPermissions(logical = Logical.OR, value = {"2","3"})
     Msg create(@Valid @RequestBody SignRecord signRecord) {
         signRecord.setBid(null);
         signRecordService.updateOne(signRecord);
         return MsgUtil.success();
     }
     @PutMapping
     @ApiOperation("修改一个签到")
//     @RequiresPermissions(logical = Logical.OR, value = {"2","3"})
     void update(@RequestBody SignRecord signRecord){

         if(signRecordRepository.existsById(signRecord.getBid()))
         signRecordService.updateOne(signRecord);
         else throw new ErrDataException(MsgEnum.ERROR_DATA_EXCEPT);
     }
     @DeleteMapping("/")
     @ApiOperation("删除一个签到")
//     @RequiresPermissions(logical = Logical.OR, value = {"3"})
         void delete(String id){
         signRecordService.deleteOne(id);
     }
}

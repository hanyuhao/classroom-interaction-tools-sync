package club.web51.classroom_tool.Controllers.SystemController;

import club.web51.classroom_tool.Config.UserAuthToken;
import club.web51.classroom_tool.Config.UserAuthenticationToken;
import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.DTO.LoginUser;
import club.web51.classroom_tool.DTO.User;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Services.UserService;
import club.web51.classroom_tool.Utils.ConvertUtil;
import club.web51.classroom_tool.Utils.JwtUtil;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;

import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Classname UserController
 * @Description TODO
 * @Date 2020/10/28 20:33
 * @Created by HanYuHao
 */

@Api(value = "用户中心",tags={"用户操作接口"})
@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    UserService userService;
    @Resource
    JwtUtil jwtUtil;

    @GetMapping
    @ApiOperation("查询已经登录的用户信息")
    public Object user_index(){
        Subject currentUser = SecurityUtils.getSubject();
        String token = (String) currentUser.getPrincipal();
        LoginUser loginUser = (LoginUser) jwtUtil.getUserByToken(token);
        return userService.findObjectByUserName(loginUser.getUsername());
    }

    @PostMapping("login")
    @ApiOperation("用户登录")
    public Msg login(@RequestBody LoginUser loginUser){
        /*
         *
         * 用json传入LoginUser 登录
         */

        Subject currentUser = SecurityUtils.getSubject();
        UserAuthToken authToken;

        if(!currentUser.isAuthenticated()){
            if(ObjectUtil.isNull(loginUser)){
                throw new ErrDataException(MsgEnum.ERROR_USER_LOGIN_EXIST);
            }
            if(ObjectUtil.isNull(loginUser.getJizhumima())){
                loginUser.setJizhumima(false);
            }
            authToken = new UserAuthToken(jwtUtil.getTokenByLoginUser(loginUser));
            authToken.setPassword(ConvertUtil.setToChar_(loginUser.getPassword()));
            currentUser.login(authToken);
            authToken.setPassword(null);
        }else{
            return MsgUtil.get(MsgEnum.ERROR_LOGINED);
        }
            return MsgUtil.success("登录成功",authToken);


    }

    @PostMapping("logout")
    @ApiOperation("用户退出")
    public Msg loginout(){
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return MsgUtil.success();
    }
}

package club.web51.classroom_tool.Controllers.SystemController;

import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Utils.ObjectUtil;
import com.alibaba.druid.sql.visitor.functions.If;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Pageable;
import club.web51.classroom_tool.DO.sysDO.Setting;
import club.web51.classroom_tool.Services.SettingService;
/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@RestController
@RequestMapping("/setting")
public class SettingController {

    final
    SettingService settingService;

    public SettingController(SettingService settingService) {
        this.settingService = settingService;
    }

    /**
     * 根据Setting的字段,自动生成条件,字段的值为null不生成条件
     * http://localhost:8080/user/?id=1
     * @param setting 实体对象
     * @param pageable 分页/排序对象
     * @return 返回的是实体,里面涵盖分页信息及状态码
     */
     @GetMapping
     ResponseEntity<Object> search(Setting setting, Pageable pageable) {
             return settingService.search(setting, pageable);
     }
     @PostMapping
     void create(Setting setting) {
         if(ObjectUtil.isNull(setting)){
             throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
         }
         setting.setId(null);
         settingService.updateOne(setting);
     }
     @PutMapping
     void update(Setting setting){
         if(ObjectUtil.isNull(setting) || ObjectUtil.isNull(setting.getId())){
             throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
         }
         settingService.updateOne(setting);
     }
     @DeleteMapping("/")
     void delete(String name){
         settingService.deleteByName(name);
     }
}

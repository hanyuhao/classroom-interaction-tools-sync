package club.web51.classroom_tool.Enums;

/**
 * @Classname TypeEnum
 * @Description TODO
 * @Date 2020/10/29 15:37
 * @Created by HanYuHao
 */
public enum TypeEnum {
    SIGN_IN(1L,"签到记录"),
    SYSTEM(2L,"操作记录"),

    ;

    private Long id;
    private String name;

    TypeEnum(long id, String name) {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

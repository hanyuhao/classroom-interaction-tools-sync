package club.web51.classroom_tool.DTO;

import club.web51.classroom_tool.DO.dataDO.SignRecord;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @Classname signRecordDTO
 * @Description TODO
 * @Date 2020/11/2 16:18
 * @Created by HanYuHao
 */
public class SignRecordDTO extends SignRecord {
    private Boolean isvalid;

    public Boolean getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Boolean isvalid) {
        this.isvalid = isvalid;
    }

    public SignRecordDTO(SignRecord signRecord){
        BeanUtils.copyProperties(signRecord,this);
        if(getDeadline().before(new Date())){
            setIsvalid(true);
        }else{
            setIsvalid(false);
        }
    }
}

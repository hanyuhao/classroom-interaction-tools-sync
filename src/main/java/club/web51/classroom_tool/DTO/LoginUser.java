package club.web51.classroom_tool.DTO;

import java.io.Serializable;

/**
 * @Classname LoginUser
 * @Description TODO
 * @Date 2020/10/29 14:13
 * @Created by HanYuHao
 */
public class LoginUser implements Serializable {

    private Long id;
    private Boolean jizhumima;
    private String username;
    private Long telephone;
    private String password;
    private Integer type;


    public LoginUser() {
    }

    public LoginUser(Long id, Boolean jizhumima, String username, Long telephone, String password, Integer type) {
        this.id = id;
        this.jizhumima = jizhumima;
        this.username = username;
        this.telephone = telephone;
        this.password = password;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getJizhumima() {
        return jizhumima;
    }

    public void setJizhumima(Boolean jizhumima) {
        this.jizhumima = jizhumima;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getTelephone() {
        return telephone;
    }

    public void setTelephone(Long telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "id=" + id +
                ", jizhumima=" + jizhumima +
                ", username='" + username + '\'' +
                ", telephone=" + telephone +
                ", password='" + password + '\'' +
                ", type=" + type +
                '}';
    }
}

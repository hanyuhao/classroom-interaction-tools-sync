package club.web51.classroom_tool.DTO;

/**
 * @Classname User
 * @Description 用户登录数据类
 * @Date 2020/10/28 20:00
 * @Created by HanYuHao
 */
public class User {
    public static final Integer STUDENT = 1;
    public static final Integer TEACHER = 2;
    public static final Integer OTHER = 3;
    private String username;
    private Long telephone;
    private String password;
    private Integer type;

    public User() {
    }

    public User(String username, String password, Integer type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public User(Long telephone, String password, Integer type) {
        this.telephone = telephone;
        this.password = password;
        this.type = type;
    }

    public User(String username, Long telephone, String password, Integer type) {
        this.username = username;
        this.telephone = telephone;
        this.password = password;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getTelephone() {
        return telephone;
    }

    public void setTelephone(Long telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                '}';
    }
}

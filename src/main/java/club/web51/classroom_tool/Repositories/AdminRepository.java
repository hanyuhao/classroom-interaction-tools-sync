package club.web51.classroom_tool.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroom_tool.DO.roleDO.Admin;

import java.util.Optional;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Repository
public interface AdminRepository extends JpaRepository<Admin,Long>,JpaSpecificationExecutor<Admin>{
    public Optional<Admin> findAdminByUsername(String username);
}
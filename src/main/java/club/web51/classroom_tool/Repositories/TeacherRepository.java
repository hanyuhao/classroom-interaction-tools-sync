package club.web51.classroom_tool.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroom_tool.DO.roleDO.Teacher;

import java.util.Optional;

/**
 * 服务类
 * @author HanYuhao
 * @since 1.0.0
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Long>,JpaSpecificationExecutor<Teacher>{
    public Optional<Teacher> findByUsername(String username);
}
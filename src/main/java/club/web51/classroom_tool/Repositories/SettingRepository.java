package club.web51.classroom_tool.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroom_tool.DO.sysDO.Setting;

/**
 * 服务类
 * @author HanYuHao
 * @since 1.0.0
 */
@Repository
public interface SettingRepository extends JpaRepository<Setting,String>,JpaSpecificationExecutor<Setting>{
    public Boolean existsSettingByName(String name);
    public void deleteByName(String name);
}
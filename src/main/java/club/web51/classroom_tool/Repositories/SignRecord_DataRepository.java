package club.web51.classroom_tool.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import club.web51.classroom_tool.DO.dataDO.SignRecord_Data;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Repository
public interface SignRecord_DataRepository extends JpaRepository<SignRecord_Data,Long>,JpaSpecificationExecutor<SignRecord_Data>{
    Boolean existsByBidAndSid(String bid, Long sid);
}
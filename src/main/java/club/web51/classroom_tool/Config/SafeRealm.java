package club.web51.classroom_tool.Config;

import club.web51.classroom_tool.DO.roleDO.Admin;
import club.web51.classroom_tool.DO.roleDO.Student;
import club.web51.classroom_tool.DO.roleDO.Teacher;
import club.web51.classroom_tool.DTO.LoginUser;
import club.web51.classroom_tool.DTO.User;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Enums.UserTypeEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.AdminRepository;
import club.web51.classroom_tool.Repositories.StudentRepository;
import club.web51.classroom_tool.Repositories.TeacherRepository;
import club.web51.classroom_tool.Services.UserService;
import club.web51.classroom_tool.Utils.ConvertUtil;
import club.web51.classroom_tool.Utils.JwtUtil;
import club.web51.classroom_tool.Utils.SFWhere;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * @Classname SafeRealm
 * @Description TODO
 * @Date 2020/10/28 19:27
 * @Created by HanYuHao
 */
public class SafeRealm extends AuthorizingRealm {
//    @Resource
//    private StudentRepository studentRepository;
//    @Resource
//    private TeacherRepository teacherRepository;
//    @Resource
//    private AdminRepository adminRepository;
    @Resource
    private JwtUtil jwtUtil;
    @Resource
    private UserService userService;

    /**
     * 由于系统暂时用不到RBAC模型，做基于用户类型的权限处理
     * @param principalCollection
     * @return null
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        String token = principalCollection.getPrimaryPrincipal().toString();

        if(!jwtUtil.validateToken(token)){
            throw new ErrDataException(MsgEnum.ERROR_USER_DATA_INVAILD);
        }

        LoginUser u = (LoginUser) jwtUtil.getUserByToken(token);

//        info.addStringPermission("1");
//        info.addStringPermission("2");
//        info.addStringPermission("3");
//        if(1 == 1){
//            return info;
//        }

        //System.out.println("执行授权校验");
        if(u.getType().equals(UserTypeEnum.STUDENT.getType())){
            //System.out.println("获得学生权限");


            info.addStringPermission(UserTypeEnum.STUDENT.getRoot().toString());
            return info;
        }
        if(u.getType().equals(UserTypeEnum.TEACHER.getType())){
            //System.out.println("获得教师权限");

            info.addStringPermission(UserTypeEnum.TEACHER.getRoot().toString());
            return info;
        }

        if(u.getType().equals(UserTypeEnum.ADMIN.getType())){
            //System.out.println("获得教师权限");

            info.addStringPermission(UserTypeEnum.ADMIN.getRoot().toString());
            return info;
        }


        return null;
    }

    //TODO 还需要完成手机号登录
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        /**
         * 获取Token中的用户信息,未登录返回空
         */

        if(Objects.isNull(authenticationToken)){
            return null;
        }
        if(Objects.isNull(authenticationToken.getCredentials())){
            return null;
        }
//        String password = ConvertUtil.getToString(authenticationToken.getCredentials());

        String token = (String) authenticationToken.getPrincipal();
        if(!jwtUtil.validateToken(token)){
            throw new ErrDataException(MsgEnum.ERROR_USER_DATA_INVAILD);
        }
        UserAuthToken userAuthToken = (UserAuthToken)authenticationToken;
//        String password = ConvertUtil.getToString(userAuthToken.getPassword());
        LoginUser loginUser = jwtUtil.getUserByToken(token);


//        loginUser.setUsername(userauthenticationToken.getUsername());
//        loginUser.setPassword(ConvertUtil.getToString(userauthenticationToken.getPassword()));
        //TODO 验证用户信息
        if(Objects.isNull(loginUser.getType())){
            return null;
        }
        else if(loginUser.getType().equals(UserTypeEnum.ADMIN.getType())){
            Admin admin = (Admin) userService.findObjectByUserName(loginUser.getUsername(),UserTypeEnum.ADMIN.getType());
            if (!admin.getPassword().equals(loginUser.getPassword())){
                return null;
            }
        }
        else if(loginUser.getType().equals(UserTypeEnum.TEACHER.getType())){
            Teacher teacher = (Teacher) userService.findObjectByUserName(loginUser.getUsername(),UserTypeEnum.TEACHER.getType());
            if (!teacher.getPassword().equals(loginUser.getPassword())){
                return null;
            }
        }
        else if(loginUser.getType().equals(UserTypeEnum.STUDENT.getType())){
            Student student = (Student) userService.findObjectByUserName(loginUser.getUsername(),UserTypeEnum.STUDENT.getType());
            if (!student.getPassword().equals(loginUser.getPassword())){
                return null;
            }
        }else{
            return null;
        }
        /**
         *  如果登录了，设置返回认证的信息
         */

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo();
        authenticationInfo.setCredentials(token);
        if(loginUser.getType().equals(UserTypeEnum.TEACHER.getType())){
            authenticationInfo.setPrincipals(new SimplePrincipalCollection(token, this.getName()));
        }
           // authenticationInfo.setCredentials(teacher);
        if(loginUser.getType().equals(UserTypeEnum.STUDENT.getType())){
            authenticationInfo.setPrincipals(new SimplePrincipalCollection(token, this.getName()));
        }
        if(loginUser.getType().equals(UserTypeEnum.ADMIN.getType())){
            authenticationInfo.setPrincipals(new SimplePrincipalCollection(token, this.getName()));
        }
          //  authenticationInfo.setCredentials(student);
        return authenticationInfo;
    }


    /**
     * 使得系统支持自定义Token
     * @param token
     * @return
     */

    @Override
    public boolean supports(AuthenticationToken token){
        return token instanceof UserAuthToken;
    }
}

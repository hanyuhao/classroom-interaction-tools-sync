package club.web51.classroom_tool.Config;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @Classname UserAuthToken
 * @Description TODO
 * @Date 2020/11/3 8:33
 * @Created by HanYuHao
 */
public class UserAuthToken extends UsernamePasswordToken implements AuthenticationToken {
    private final String token;

    public UserAuthToken(String token) {
        super();
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;

    }

    @Override
    public String toString() {
        return token;
    }
}

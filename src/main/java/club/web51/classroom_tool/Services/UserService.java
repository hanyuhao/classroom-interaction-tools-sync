package club.web51.classroom_tool.Services;

import club.web51.classroom_tool.DO.roleDO.Admin;
import club.web51.classroom_tool.DO.roleDO.Teacher;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Enums.UserTypeEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.AdminRepository;
import club.web51.classroom_tool.Repositories.StudentRepository;
import club.web51.classroom_tool.Repositories.TeacherRepository;
import club.web51.classroom_tool.Utils.ObjectUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Classname UserService
 * @Description 用户服务类，用于检查用户表数据
 * @Date 2020/11/1 16:56
 * @Created by HanYuHao
 */
@Service
public class UserService {
    @Resource
    private StudentRepository studentRepository;
    @Resource
    private TeacherRepository teacherRepository;
    @Resource
    private AdminRepository adminRepository;

    /**
     * 通过ID，返回系统角色
     * @param id
     * @return
     */
    public Integer findRoleById(Long id){
        if(studentRepository.existsById(id)){
            return UserTypeEnum.STUDENT.getType();
        }

        if(teacherRepository.existsById(id)){
            return UserTypeEnum.TEACHER.getType();
        }

        if(adminRepository.existsById(id)){
            return UserTypeEnum.ADMIN.getType();
        }

        throw new ErrDataException(MsgEnum.ERROR_USER_DATA_ERR);
    }

    /**
     * 通过ID，返回对应实体
     * @param id
     * @return
     */
    public Object findObjectById(Long id){
        if(studentRepository.findById(id).isPresent()){
            return studentRepository.findById(id).get();
        }

        if(teacherRepository.findById(id).isPresent()){
            return teacherRepository.findById(id).get();
        }

        if(adminRepository.findById(id).isPresent()){
            return adminRepository.findById(id).get();
        }

        throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
    }

    public Object findObjectByUserName(String username){
        if(adminRepository.findAdminByUsername(username).isPresent()){
            return adminRepository.findAdminByUsername(username);
        }

        if(teacherRepository.findByUsername(username).isPresent()){
            return teacherRepository.findByUsername(username);
        }
        if(studentRepository.findByUsername(username).isPresent()){
            return studentRepository.findByUsername(username);
        }
        throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
    }

    public Object findObjectByUserName(String username,Integer type){

        if(adminRepository.findAdminByUsername(username).isPresent() && type.equals(UserTypeEnum.ADMIN.getType())){
            return adminRepository.findAdminByUsername(username).get();
        }

        if(teacherRepository.findByUsername(username).isPresent() && type.equals(UserTypeEnum.TEACHER.getType())){
            return teacherRepository.findByUsername(username).get();
        }
        if(studentRepository.findByUsername(username).isPresent() && type.equals(UserTypeEnum.STUDENT.getType())){
            return studentRepository.findByUsername(username).get();
        }
        throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
    }

}

package club.web51.classroom_tool.Services;

import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Utils.BeanPlusUtils;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroom_tool.Repositories.TeacherRepository;
import club.web51.classroom_tool.Utils.SFWhere;
import club.web51.classroom_tool.DO.roleDO.Teacher;

/**
 * 服务类
 * @author HanYuhao
 * @since 1.0.0
 */
@Service
public class TeacherService {
    @Autowired
    TeacherRepository teacherRepository;
    /**
     * 根据Teacher的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(teacher).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param teacher     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public ResponseEntity<Object> search(Teacher teacher, Pageable pageable) {
        Page<Teacher> all = teacherRepository.findAll(SFWhere.and(teacher)
                //.equal(user.getId() > 0, "id", user.getId())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                .like(teacher.getName() != null, "name", "%" + teacher.getName() + "%")
                //....
                .build(), pageable);
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    public Msg updateOne(Teacher teacher){
        if(ObjectUtil.isNull(teacher)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        if(ObjectUtil.isNull(teacher.getId())){
            teacherRepository.save(teacher);
            return MsgUtil.success();
        }
        Teacher teacher1 = teacherRepository.getOne(teacher.getId());
        BeanPlusUtils.copyPropertiesIgnoreNull(teacher,teacher1);
        teacherRepository.save(teacher1);
        return MsgUtil.success();
    }

    public Msg deleteOne(Long id){
        if(ObjectUtil.isNull(id)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        teacherRepository.deleteById(id);
        return MsgUtil.success();
    }
}

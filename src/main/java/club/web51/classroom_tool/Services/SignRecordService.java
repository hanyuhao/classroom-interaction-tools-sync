package club.web51.classroom_tool.Services;

import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.DTO.SignRecordDTO;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Utils.BeanPlusUtils;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired ; 
import club.web51.classroom_tool.Repositories.SignRecordRepository;
import club.web51.classroom_tool.Utils.SFWhere;
import club.web51.classroom_tool.DO.dataDO.SignRecord;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Service
public class SignRecordService {
    @Autowired
    SignRecordRepository signRecordRepository;
    /**
     * 根据SignRecord的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(signRecord).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param signRecord     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public ResponseEntity<Object> search(SignRecord signRecord, Pageable pageable) {
        Page<SignRecord> all = signRecordRepository.findAll(SFWhere.and(signRecord)
                //.equal(user.getId() > 0, "id", user.getId())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                //.like(字段值的判断 != null, "字段名称", "%" + 字段值 + "%")
                //....
                .build(), pageable);
        List<SignRecord> signRecords = all.getContent();
        List<SignRecordDTO> signRecordDTOS = signRecords.stream().map((SignRecord s) -> {
            return (SignRecordDTO)s;
        }).collect(Collectors.toList());
        Page<SignRecordDTO> page = new PageImpl<SignRecordDTO>(signRecordDTOS, all.getPageable(), all.getTotalElements());
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    public Msg updateOne(SignRecord signRecord){
        if(ObjectUtil.isNull(signRecord)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        if(ObjectUtil.isNull(signRecord.getBid())){
            signRecordRepository.save(signRecord);
            return MsgUtil.success();
        }
        SignRecord signRecord1 = signRecordRepository.getOne(signRecord.getBid());
        BeanPlusUtils.copyPropertiesIgnoreNull(signRecord,signRecord1);
        signRecordRepository.save(signRecord1);
        return MsgUtil.success();
    }

    public Msg deleteOne(String id){
        if(ObjectUtil.isNull(id)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        signRecordRepository.deleteById(id);
        return MsgUtil.success();
    }
}

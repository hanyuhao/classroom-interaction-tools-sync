package club.web51.classroom_tool.Services;

import club.web51.classroom_tool.DO.collDO.Academy;
import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.Enums.MsgEnum;
import club.web51.classroom_tool.Exceptions.ErrDataException;
import club.web51.classroom_tool.Repositories.AcademyRepository;
import club.web51.classroom_tool.Utils.BeanPlusUtils;
import club.web51.classroom_tool.Utils.MsgUtil;
import club.web51.classroom_tool.Utils.ObjectUtil;
import club.web51.classroom_tool.Utils.SFWhere;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 服务类
 * @author Hanyuhao
 * @since 1.0.0
 */
@Service
public class AcademyService {
    @Autowired
    AcademyRepository academyRepository;
    @Resource
    TeacherService teacherService;
    /**
     * 根据Academy的字段自动生成条件,字段值为null不生成条件
     * 如果是数值型的字段,前端不传入值,默认是0,例如ID的类型是Long,如果不传值,默认是0
     * 可以自己设置下SFWhere.and(academy).equal(实体.getId()>0,"id",实体.getId()).build()
     * @param academy     实体对象
     * @param pageable 分页对象
     * @return 返回分页\状态码
     */
    public ResponseEntity<Object> search(Academy academy, Pageable pageable) {


        Page<Academy> all = academyRepository.findAll(SFWhere.and(academy)
//                .equal(root, "id", teacher.getAcademy_id())
                //.in(true, "userName", longs)
                //.equal(!字段值的判断.equals("") && 字段值的判断 != null, "字段名称", 字段值)
                .like(academy.getName() != null, "name", "%" + academy.getName() + "%")
                //....
                .build(), pageable);
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    public Msg updateOne(Academy academy){
        if(ObjectUtil.isNull(academy)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        if(ObjectUtil.isNull(academy.getId())){
            academyRepository.save(academy);
            return MsgUtil.success();
        }
        Academy academy1 = academyRepository.getOne(academy.getId());
        BeanPlusUtils.copyPropertiesIgnoreNull(academy,academy1);
        academyRepository.save(academy1);
        return MsgUtil.success();
    }

    public Msg deleteOne(Long id){
        if(ObjectUtil.isNull(id)){
            throw new ErrDataException(MsgEnum.ERROR_DATA_EMPTY);
        }
        academyRepository.deleteById(id);
        return MsgUtil.success();
    }
}

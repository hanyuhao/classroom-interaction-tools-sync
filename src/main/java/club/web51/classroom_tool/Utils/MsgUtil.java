package club.web51.classroom_tool.Utils;

import club.web51.classroom_tool.DO.sysDO.Msg;
import club.web51.classroom_tool.Enums.MsgEnum;

/**
 * @Classname MsgUtil
 * @Description TODO
 * @Date 2020/10/28 17:42
 * @Created by HanYuHao
 */
public class MsgUtil {

    public static Msg get(MsgEnum msgEnum){
        return new Msg(msgEnum);
    }

    public static  Msg get(MsgEnum msgEnum,Object note){
        return new Msg(msgEnum,note);
    }

    public  static Msg success(){
        return new Msg(MsgEnum.OK);
    }
    public  static Msg success(String description){
        return new Msg(MsgEnum.OK.getCode(),description,null);
    }
    public  static Msg success(String description,Object note){
        return new Msg(MsgEnum.OK.getCode(),description,note);
    }
    public static  Msg success(Object note){
        return new Msg(MsgEnum.OK,note);
    }

    public static  Msg fail(){
        return new Msg(MsgEnum.ERROR);
    }

    public static  Msg fail(Object note){
        return new Msg(MsgEnum.ERROR,note);
    }
}

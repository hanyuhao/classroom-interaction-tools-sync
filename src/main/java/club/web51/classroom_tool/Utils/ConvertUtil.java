package club.web51.classroom_tool.Utils;

/**
 * @Classname ConvertUtil
 * @Description TODO
 * @Date 2020/10/28 21:41
 * @Created by HanYuHao
 */
public class ConvertUtil {
    /**
     * Shiro Credential 获取专用
     * @param obj
     * @return
     */
    public static String getToString(Object obj){
        char[] str = (char[]) obj;
        return String.copyValueOf(str);
    }

    public static char[] setToChar_(String password){
        return password.toCharArray();
    }
}

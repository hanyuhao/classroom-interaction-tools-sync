package club.web51.classroom_tool;

import club.web51.classroom_tool.Utils.ProfileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassroomToolApplication {

    public static void main(String[] args) {

        SpringApplication.run(ClassroomToolApplication.class, args);
        switch (ProfileUtil.getActiveProfile()){
            case "dev":{
                String port = ProfileUtil.getPort();
                System.out.println("您正处于Dev环境中");
                System.out.println("Dev环境下，可输入 http://localhost:"+port+"/swagger-ui.html 查看接口文档");
                System.out.println("Dev环境下，可输入 http://localhost:"+port+"/druid 查看数据库监控");
                System.out.println("druid账号和密码均为admin");
                break;
            }

            case "prod":{
                System.out.println("您正处于Prod环境中");
                break;
            }

            default:{
                System.out.println("请采用正确的环境配置启动！");
                System.exit(-1);
                break;
            }
        }
    }

}

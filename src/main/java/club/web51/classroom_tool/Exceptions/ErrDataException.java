package club.web51.classroom_tool.Exceptions;

import club.web51.classroom_tool.Enums.MsgEnum;

/**
 * @Classname NoDataException
 * @Description TODO
 * @Date 2020/10/28 15:19
 * @Created by HanYuHao
 */

public class ErrDataException extends RuntimeException{
    private final Integer code;
    public ErrDataException() {
        super(MsgEnum.ERROR.getDescription());
        this.code = MsgEnum.ERROR.getCode();
    }
    public ErrDataException(String message) {
        super(message);
        this.code = MsgEnum.ERROR.getCode();
    }

    public ErrDataException(MsgEnum msgEnum) {
        super(msgEnum.getDescription());
        this.code = msgEnum.getCode();
    }


    public Integer getCode() {
        return code;
    }
}

# 课堂互动工具-自同步

#### 介绍
本项目是单端项目代码。

#### 进度
1.10/28 完成项目的框架搭建，所有类的增删改查，以及基于Shiro的学生、教师登录，权限控制。

#### 软件架构
SpringBoot+Spring-Data-Jpa+Shrio

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx
